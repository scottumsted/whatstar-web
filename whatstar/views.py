import random

__author__ = 'scottumsted'

from flask.globals import request
from flask import jsonify, json, redirect
from flask.templating import render_template
from whatstar import app, wdb

def _padded_jsonify(callback, *args, **kwargs):
    content = str(callback) + '(' + json.dumps(dict(*args, **kwargs)) + ')'
    return app.response_class(content, mimetype='application/json')

@app.route('/')
def landing():
    try:
        twits = wdb.get_twit_by_status('New')
        background_image = 'unsplash%d.jpg'%(random.randint(0,9))
        content = render_template('index.html', twits=twits,background_image=background_image)
    except Exception, e:
        content = 'Error'
    return content

@app.route('/twit', methods=['POST'])
def add_twit():
    result = {'successful': True, 'data': []}
    callback = request.args.get('callback', None)
    try:
        if request.json is None:
            result['successful'] = False
            result['data'] = 'Not JSON'
        else:
            twit = json.loads(request.json)
            id = wdb.add_twit(twit)
            if id is not None and id > 0:
                result['data'] = id
            else:
                result['data'] = -1
                result['successful'] = False
        if callback is None:
            result = jsonify(result)
        else:
            result = _padded_jsonify(callback, result)
    except Exception, e:
        result = jsonify({'successful': False, 'data': 'Unable to process request'})
    return result

@app.route('/twit/before/<twit_id>',methods=['GET'])
def get_twit_before_twit_id(twit_id):
    result = {'successful': True, 'data': []}
    callback = request.args.get('callback', None)
    try:
        status = request.args.get('status', 'New')
        result['data'] = wdb.get_twit_before_twit_id(status, twit_id)
        if callback is None:
            result = jsonify(result)
        else:
            result = _padded_jsonify(callback, result)
    except Exception, e:
        result = jsonify({'succcessful': False, 'data': 'Unable to process request'})
    return result

@app.route('/twit', methods=['GET'])
def get_twit_by_status():
    result = {'successful': True, 'data': []}
    callback = request.args.get('callback', None)
    try:
        status = request.args.get('status', 'New')
        result['data'] = wdb.get_twit_by_status(status)
        if callback is None:
            result = jsonify(result)
        else:
            result = _padded_jsonify(callback, result)
    except Exception, e:
        result = jsonify({'succcessful': False, 'data': 'Unable to process request'})
    return result

@app.route('/twit/id/<id>', methods=['GET'])
def get_twit_by_id(id):
    result = {'successful': True, 'data': []}
    callback = request.args.get('callback', None)
    try:
        result['data'] = wdb.get_twit_by_id(id)
        if callback is None:
            result = jsonify(result)
        else:
            result = _padded_jsonify(callback, result)
    except Exception, e:
        result = jsonify({'successful': False, 'data': 'Unable to process request'})
    return result

@app.route('/twit/term/<term_id>', methods=['GET'])
def get_twit_by_term_id(term_id):
    result = {'successful': True, 'data': []}
    callback = request.args.get('callback', None)
    try:
        result['data'] = wdb.get_twit_by_term_id(term_id)
        if callback is None:
            result = jsonify(result)
        else:
            result = _padded_jsonify(callback, result)
    except Exception, e:
        result = jsonify({'successful': False, 'data': 'Unable to process request'})
    return result

@app.route('/twit/star', methods=['PUT'])
def update_twit_star():
    result = {'successful': True, 'data': []}
    callback = request.args.get('callback', None)
    try:
        if request.json is None:
            result['successful'] = False
            result['data'] = 'Not JSON'
        else:
            twit = json.loads(request.json)
            id = wdb.update_twit_star(twit)
            if id is not None and id > 0:
                result['data'] = id
            else:
                result['data'] = -1
                result['successful'] = False
        if callback is None:
            result = jsonify(result)
        else:
            result = _padded_jsonify(callback, result)
    except Exception, e:
        result = jsonify({'successful': False, 'data': 'Unable to process request'})
    return result

@app.route('/twit/status', methods=['PUT'])
def update_twit_status():
    result = {'successful': True, 'data': []}
    callback = request.args.get('callback', None)
    try:
        if request.json is None:
            result['successful'] = False
            result['data'] = 'Not JSON'
        else:
            twit = json.loads(request.json)
            id = wdb.update_twit_status(twit)
            if id is not None and id > 0:
                result['data'] = id
            else:
                result['data'] = -1
                result['successful'] = False
        if callback is None:
            result = jsonify(result)
        else:
            result = _padded_jsonify(callback, result)
    except Exception, e:
        result = jsonify({'successful': False, 'data': 'Unable to process request'})
    return result

@app.route('/twit/max_twit_id/<term_id>', methods=['GET'])
def get_max_twit_id(term_id):
    result = {'successful': True, 'data': []}
    callback = request.args.get('callback', None)
    try:
        result['data'] = int(wdb.get_max_twit_id(term_id))
        if callback is None:
            result = jsonify(result)
        else:
            result = _padded_jsonify(callback, result)
    except Exception, e:
        result = jsonify({'successful': False, 'data': 'Unable to process request'})
    return result

@app.route('/term', methods=['GET'])
def get_terms():
    result = {'successful': True, 'data': []}
    callback = request.args.get('callback', None)
    try:
        result['data'] = wdb.get_terms()
        if callback is None:
            result = jsonify(result)
        else:
            result = _padded_jsonify(callback, result)
    except Exception, e:
        result = jsonify({'successful': False, 'data': 'Unable to process request'})
    return result

