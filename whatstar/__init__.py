__author__ = 'scottumsted'
from flask import Flask
app = Flask(__name__)
app.config.from_envvar('WHATSTAR_WEB_SETTINGS', silent=False)
from data.models import WhichstarModel
wdb = WhichstarModel()
import whatstar.views
from utils import get_pretty_date_time_string
app.jinja_env.globals.update(get_pretty_date_time_string=get_pretty_date_time_string)
