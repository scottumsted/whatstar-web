drop table twit;
create table twit (
id serial primary key not null,
term_id numeric,
term text,
twit_id numeric unique,
twit_text text,
twit_screen_name text,
twit_created_at timestamp,
twit_lat numeric,
twit_lon numeric,
twit_media_url text,
twit_media_width numeric,
twit_media_height numeric,
twit_profile_image_url text,
twit_retweet_count numeric,
twit_favorite_count numeric,
created timestamp,
status text,
star text,
image text
);

drop table term;
create table term (
id serial primary key not null,
term text,
term_type text,
created timestamp
);