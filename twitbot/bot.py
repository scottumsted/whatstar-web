__author__ = 'scottumsted'
import requests
import sys, json, os, yaml
from twitter import *


class Bot():
    def __init__(self):
        yaml_file = open(os.getenv('WHATSTAR_WEB_YAML', ''))
        self.settings = yaml.load(yaml_file)
        yaml_file.close()
        self.twit = Twitter(
            auth=OAuth(self.settings['WHATSTAR_BOT_ACCESS_TOKEN'],
                       self.settings['WHATSTAR_BOT_ACCESS_SECRET'],
                       self.settings['WHATSTAR_BOT_API_KEY'],
                       self.settings['WHATSTAR_BOT_API_SECRET'])
        )

    def start_pull(self):
        print 'startPull'
        terms = self.get_terms()
        for term in terms:
            if term['term_type'] == 'mention':
                self.pull_mentions(term)
            elif term['term_type'] == 'query':
                self.pull_query(term)
            else:
                print 'unknown term type:'+str(term)

    def pull_mentions(self, term):
        max_twit_id = self.get_max_twit_id(term['id'])
        if max_twit_id is not None:
            result = self.twit.statuses.mentions_timeline(count=200, since_id=max_twit_id)
        else:
            result = self.twit.statuses.mentions_timeline(count=200)
        for status in result:
            twit = self.process_status(term, status)
            self.post_twit(twit)

    def pull_query(self, term):
        max_twit_id = self.get_max_twit_id(term['id'])
        if max_twit_id is not None:
            result = self.twit.search.tweets(q=term['term'], count=200, since_id=max_twit_id)
        else:
            result = self.twit.search.tweets(q=term['term'], count=200, since_id=max_twit_id)
        for status in result['statuses']:
            twit = self.process_status(term, status)
            self.post_twit(twit)

    def process_status(self, term, status):
        twit = {
            'twit_id': status['id'],
            'twit_text': status['text'],
            'twit_screen_name': status['user']['screen_name'],
            'twit_created_at': status['created_at'],
            'term_id': term['id'],
            'term': term['term'],
            'twit_lat': 0,
            'twit_lon': 0,
            'twit_favorite_count': status['favorite_count'],
            'twit_retweet_count': status['retweet_count'],
            'twit_media_url': '',
            'twit_profile_image_url': status['user']['profile_image_url'].replace('_normal', ''),
            'twit_media_width': 0,
            'twit_media_height': 0
        }
        if status['coordinates'] is not None and status['coordinates']['type'] == 'Point':
            twit['twit_lat'] = status['coordinates']['coordinates'][1]
            twit['twit_lon'] = status['coordinates']['coordinates'][0]
        if 'entities' in status and 'media' in status['entities']:
            for item in status['entities']['media']:
                twit['twit_media_url'] = item['media_url']
                twit['twit_media_width'] = item['sizes']['medium']['w']
                twit['twit_media_height'] = item['sizes']['medium']['h']
                break

        return twit

    def get_max_twit_id(self,term_id):
        url = self.settings['WHATSTAR_BOT_API_URL']+'/twit/max_twit_id/'+str(term_id)
        response = requests.get(url)
        j = response.json()
        if j['successful'] == True:
            return response.json()['data']
        else:
            print 'get_max_twit_id: '+str(j['data'])
            return None

    def get_terms(self):
        url = self.settings['WHATSTAR_BOT_API_URL']+'/term'
        response = requests.get(url)
        j = response.json()
        if j['successful'] == True:
            return response.json()['data']
        else:
            print 'get_max_twit_id: '+str(j['data'])
            return None

    def post_twit(self, twit):
        url = self.settings['WHATSTAR_BOT_API_URL']+'/twit'
        twit_json = json.dumps(twit, ensure_ascii=False)
        headers = {'content-type': 'application/json'}
        response = requests.post(url, data=json.dumps(twit_json), headers=headers)
        rdict = response.json()
        print str(rdict)

    def get_star(self, in_twit):
        pass

    def startPost(self):
        print 'startPost'
        pass


if __name__ == '__main__':
    args = sys.argv
    if len(args) != 2 or args[1] not in ['pull', 'post']:
        print 'usage: python bot.py {pull|post}'
    else:
        if args[1] == 'pull':
            b = Bot()
            b.start_pull()
        else:
            b = Bot()
            b.start_post()