__author__ = 'scottumsted'

from datetime import datetime
from time import mktime
import logging
from data import dbpool

class WhichstarModel():

    _selection = "id, twit_id, twit_screen_name, twit_lat, twit_lon, twit_retweet_count, twit_favorite_count, twit_profile_image_url, twit_media_url, twit_text, to_char(twit_created_at,'YYYYMMDDHH24MISS') twit_created_at_string, star, image, status, to_char(created,'YYYYMMDDHH24MISS') created_string, twit_media_width, twit_media_height "

    def __init__(self):
        logging.basicConfig(level=logging.INFO)

    def update_twit_status(self, twit):
        con, cur = dbpool.get_connection()
        result = None
        try:
            sql = u"update twit set status=%(status)s where id=%(id)s returning id"
            cur.execute(sql, twit)
            if cur.rowcount > 0:
                result = cur.fetchone()['id']
        except Exception, e:
            pass
        finally:
            con.commit()
            dbpool.put_connection(con)
        return result

    def update_twit_star(self, twit):
        con, cur = dbpool.get_connection()
        result = None
        try:
            sql = u"update twit set status='Image', star=%(star)s, image=%(image)s where id=%(id)s returning id"
            cur.execute(sql, twit)
            if cur.rowcount > 0:
                result = cur.fetchone()['id']
        except Exception, e:
            pass
        finally:
            con.commit()
            dbpool.put_connection(con)
        return result

    def add_twit(self, twit):
        con, cur = dbpool.get_connection()
        result = None
        try:
            sql = u"insert into twit (twit_id, twit_text, twit_screen_name, twit_created_at, status, created, term_id, term, twit_lat, twit_lon, twit_media_url, twit_profile_image_url, twit_favorite_count, twit_retweet_count, twit_media_width, twit_media_height) "
            sql += u"values (%(twit_id)s, %(twit_text)s, %(twit_screen_name)s, %(cvt_date)s, 'New', now(), %(term_id)s, %(term)s, %(twit_lat)s, %(twit_lon)s, %(twit_media_url)s, %(twit_profile_image_url)s, %(twit_favorite_count)s, %(twit_retweet_count)s, %(twit_media_width)s, %(twit_media_height)s) returning id"
            twit['cvt_date'] = datetime.strptime(twit['twit_created_at'].replace('+0000', 'UTC'), '%a %b %d %H:%M:%S %Z %Y')
            cur.execute(sql, twit)
            if cur.rowcount > 0:
                result = cur.fetchone()['id']
        except Exception, e:
            pass
        finally:
            con.commit()
            dbpool.put_connection(con)
        return result

    def get_twit_by_status(self, status):
        con, cur = dbpool.get_connection()
        result = None
        try:
            sql = "select "+self._selection+" from twit where status=%(status)s order by twit_id desc limit 100"
            cur.execute(sql, {'status': status})
            result = cur.fetchall()
        except Exception, e:
            pass
        finally:
            con.commit()
            dbpool.put_connection(con)
        return result

    def get_twit_before_twit_id(self, status, twit_id):
        con, cur = dbpool.get_connection()
        result = None
        try:
            sql = "select "+self._selection+" from twit where status=%(status)s and twit_id<%(twit_id)s order by twit_id desc limit 100"
            cur.execute(sql, {'status': status, 'twit_id': twit_id})
            result = cur.fetchall()
        except Exception, e:
            pass
        finally:
            con.commit()
            dbpool.put_connection(con)
        return result

    def get_twit_by_term_id(self, term_id):
        con, cur = dbpool.get_connection()
        result = None
        try:
            sql = "select "+self._selection+" from twit where term_id=%(term_id)s order by twit_id desc limit 100"
            cur.execute(sql, {'term_id': term_id})
            result = cur.fetchall()
        except Exception, e:
            pass
        finally:
            con.commit()
            dbpool.put_connection(con)
        return result

    def get_twit_by_id(self, id):
        con, cur = dbpool.get_connection()
        result = None
        try:
            sql = "select "+self._selection+" from twit where id=%(id)s"
            cur.execute(sql, {'id': id})
            result = cur.fetchall()
        except Exception, e:
            pass
        finally:
            con.commit()
            dbpool.put_connection(con)
        return result

    def get_max_twit_id(self, term_id):
        con, cur = dbpool.get_connection()
        result = None
        try:
            sql = "select max(twit_id) from twit where term_id=%(term_id)s"
            cur.execute(sql, {'term_id': term_id})
            result = cur.fetchone()['max']
        except Exception, e:
            pass
        finally:
            con.commit()
            dbpool.put_connection(con)
        return result

    def get_terms(self):
        con, cur = dbpool.get_connection()
        result = None
        try:
            sql = "select id, term, term_type from term"
            cur.execute(sql, {})
            result = cur.fetchall()
        except Exception, e:
            pass
        finally:
            con.commit()
            dbpool.put_connection(con)
        return result